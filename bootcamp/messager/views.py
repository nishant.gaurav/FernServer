from django.db.models import Count, Q
from django.shortcuts import get_object_or_404

from bootcamp.messager import models
from bootcamp.messager import serializers
from bootcamp.messager.fcm import send_event_via_fcm

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from itertools import chain

class LoadInbox(APIView):

    def get(self, request):
        """Load user inbox threads

        - Retrieve all of the threads that includes the user in the clients field.
        - count number of unread messages using related name receipts containing user
        - returns {"threads":[thread]}
        """
        threads = models.MessageThread.objects.filter(clients=request.user, is_active=True, membership__archived=False).annotate(
            unread_count=Count('receipts',filter=Q(receipts__recipient=request.user))
        )
        if request.user.is_superuser:
            threads2 = models.MessageThread.objects.filter(clients=request.user, is_active=True, membership__archived=True).annotate(
                unread_count=Count('receipts',filter=Q(receipts__recipient=request.user))
            ).exclude(unread_count=0)
            threads = chain(threads,threads2)
        thread_data = serializers.MessageThreadListSerializer(threads).data
        return Response({'threads':thread_data})


class LoadMessages(APIView):

    def get(self, request):
        """Load messages from thread

        - Load 30 messages by default.
        - The 'before' parameter will load the previous 30 messages relative to the date.
        - returns json {messages:[message], end:bool}
        """
        thread = models.MessageThread.objects.get(uuid_id=request.GET['id'])
        # make sure we are part of this chat before we read the messages
        if not request.user in thread.clients.all():
            return Response(status=403)
        # query for messages filter
        q = [Q(thread=thread)]
        if 'before' in request.GET:
            q.append(Q(id__lt=int(request.GET['before'])))
        # query messages matching filter
        messages = models.Message.objects.filter(*q)
        messages_data = serializers.MessageListSerializer(messages[:30]).data
        # mark any unread messages in chat as read
        thread.mark_read(request.user)
        return Response({"messages":messages_data,"end":messages.count() <= 30})


class EditMembership(APIView):

    def get(self, request):
        membership = get_object_or_404(models.Membership, user=request.user, messagethread=request.POST.get("id", ""))
        action = request.POST.get("action", "")
        if action == 'archive':
            membership.archived = True
            membership.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        elif action == 'mute':
            membership.notification_on = False
            membership.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        elif action == 'unmute':
            membership.notification_on = False
            membership.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        elif action == 'accept':
            TEXT = "We are delighted to tell you that you are now off the waitlist. Now before you start swiping, just make sure you have one holiday picture and don't shy from mentioning your job/company/college."
            membership.messagethread.add_message_text(TEXT,request.user)
            notification_serialized = {
                        "message_title":"Kudos 😃",
                        "message_body":"Your can now start swiping.",
                        "thread_id":str(membership.messagethread.uuid_id),
                        "peer_display_name":str(request.user.displayName),
                        "peer_id":str(request.user.id),
                        "peer_avatar":str(request.user.avatar),
                    }
            for client in membership.messagethread.clients.exclude(id=request.user.id):
                try:
                    client.is_new = True
                    client.swipe_enabled = True
                    client.save()
                    send_event_via_fcm(client, notification_serialized)
                except:
                    pass
            membership.archived = True
            membership.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        elif action == 'reject':
            for client in membership.messagethread.clients.exclude(id=request.user.id):
                try:
                    client.is_new = False
                    client.swipe_enabled = False
                    client.save()
                except:
                    pass
            membership.archived = True
            membership.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        return Response(status=status.HTTP_400_BAD_REQUEST)