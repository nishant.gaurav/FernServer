from django.conf.urls import url
from . import views

app_name="friendships"
urlpatterns = [
    url(
        regex=r"^like/$",
        view=views.LikeApiView.as_view(),
        name="friendship_like",
    ),
    url(
        regex=r"^pass/$",
        view=views.PassApiView.as_view(),
        name="friendship_pass",
    ),
    url(
        regex=r"^report/$",
        view=views.ReportApiView.as_view(),
        name="friendship_report",
    ),
    url(
        regex=r"^proposals/$",
        view=views.ProposalsApiView.as_view(),
        name="friendship_proposals",
    ),
    # url(regex=r"^users/$", view=all_users, name="friendship_view_users"),
    # url(
    #     regex=r"^friends/(?P<username>[\w-]+)/$",
    #     view=views.ViewFriends.as_view(),
    #     name="friendship_view_friends",
    # ),
    # url(
    #     regex=r"^friend/add/(?P<uuid>[\w-]+)/$",
    #     view=views.FriendshipAddFriend.as_view(),
    #     name="friendship_add_friend",
    # ),
    # url(
    #     regex=r"^friend/accept/(?P<friendship_request_id>\d+)/$",
    #     view=views.FriendshipAccept.as_view(),
    #     name="friendship_accept",
    # ),
    # url(
    #     regex=r"^friend/reject/(?P<friendship_request_id>\d+)/$",
    #     view=views.FriendshipReject.as_view(),
    #     name="friendship_reject",
    # ),
    # url(
    #     regex=r"^friend/cancel/(?P<friendship_request_id>\d+)/$",
    #     view=views.FriendshipCancel.as_view(),
    #     name="friendship_cancel",
    # ),
    # url(
    #     regex=r"^friend/requests/$",
    #     view=views.FriendshipRequestList.as_view(),
    #     name="friendship_request_list",
    # ),
    # url(
    #     regex=r"^friend/requests/rejected/$",
    #     view=views.FriendshipRequestListRejected.as_view(),
    #     name="friendship_requests_rejected",
    # ),
    # url(
    #     regex=r"^friend/request/(?P<friendship_request_id>\d+)/$",
    #     view=views.FriendshipRequests_detail.as_view(),
    #     name="friendship_requests_detail",
    # ),
    # url(
    #     regex=r"^followers/(?P<username>[\w-]+)/$",
    #     view=views.Followers.as_view(),
    #     name="friendship_followers",
    # ),
    # url(
    #     regex=r"^following/(?P<username>[\w-]+)/$",
    #     view=views.Following.as_view(),
    #     name="friendship_following",
    # ),
    # url(
    #     regex=r"^follower/add/(?P<followee_username>[\w-]+)/$",
    #     view=views.FollowerAdd.as_view(),
    #     name="follower_add",
    # ),
    # url(
    #     regex=r"^follower/remove/(?P<followee_username>[\w-]+)/$",
    #     view=views.FollowerRemove.as_view(),
    #     name="follower_remove",
    # ),
    # url(
    #     regex=r"^blockers/(?P<username>[\w-]+)/$",
    #     view=views.Blockers.as_view(),
    #     name="friendship_blockers",
    # ),
    # url(
    #     regex=r"^blocking/(?P<username>[\w-]+)/$",
    #     view=views.Blocking.as_view(),
    #     name="friendship_blocking",
    # ),
    # url(
    #     regex=r"^block/add/(?P<blocked_username>[\w-]+)/$",
    #     view=views.BlockAdd.as_view(),
    #     name="block_add",
    # ),
    # url(
    #     regex=r"^block/remove/(?P<blocked_username>[\w-]+)/$",
    #     view=views.BlockRemove.as_view(),
    #     name="block_remove",
    # ),
]
