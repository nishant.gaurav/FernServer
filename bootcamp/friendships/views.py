from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.conf import settings

try:
    from django.contrib.auth import get_user_model

    user_model = get_user_model()
except ImportError:
    from django.contrib.auth.models import User

    user_model = User

from django.shortcuts import render, get_object_or_404, redirect

from .exceptions import AlreadyExistsError
from .models import Friend, Follow, FriendshipRequest, Block

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics, status
from django.db.models import F, Q, IntegerField, CharField, ExpressionWrapper, Value
from django.db.models.functions import Least
from django.contrib.gis.db.models.functions import Distance
from bootcamp.users.serializers import UserSerializer, UserListSerializer
from bootcamp.messager.models import MessageThread

from datetime import timedelta
from itertools import chain


class LikeApiView(APIView):

    def get(self, request):
        to_user = get_object_or_404(user_model, id=request.GET['id'])
        from_user = request.user
        if not request.user.add_like():
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        if FriendshipRequest.objects.filter(
            from_user=to_user, to_user=from_user, viewed__isnull=True
        ).exists():
            FriendshipRequest.objects.get(
                from_user=to_user, to_user=from_user).accept()
            return Response(UserSerializer(request.user).data)#"matched"
        try:
            Friend.objects.add_friend(from_user, to_user)
        except AlreadyExistsError:
            #TODO log this error and see why it happens
            f_request = FriendshipRequest.objects.get(from_user=from_user, to_user=to_user)
            if f_request.viewed:
                f_request.rejected = f_request.viewed
                f_request.created = timezone.now()
                f_request.viewed = None
                f_request.save()
                return Response(UserSerializer(request.user).data)#"already_rejected"
            else:
                f_request.created = timezone.now()
                f_request.save()
                return Response(UserSerializer(request.user).data)#"created_updated"
        else:
            FriendshipRequest.objects.filter(from_user=to_user, to_user=from_user, viewed__isnull=False).delete()
            return Response(UserSerializer(request.user).data)#"liked"

    def post(self, request):
        to_user = get_object_or_404(user_model, id=request.POST.get("id", ""))
        from_user = request.user
        if not request.user.add_superlike():
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        if FriendshipRequest.objects.filter(
            from_user=to_user, to_user=from_user, viewed__isnull=True
        ).exists():
            FriendshipRequest.objects.get(
                from_user=to_user, to_user=from_user).accept()
            return Response(UserSerializer(request.user).data)#"matched"
        message = request.POST.get("message", "").strip()
        try:
            if len(message) > 0 and len(message) <= 200:
                Friend.objects.add_friend(from_user, to_user, message)
            else:
                Friend.objects.add_friend(from_user, to_user)
        except AlreadyExistsError:
            #TODO log this error and see why it happens
            f_request = FriendshipRequest.objects.get(from_user=from_user, to_user=to_user)
            if len(message) >= 20 and len(message) <= 200:
                f_request.message = message
            if request.viewed:
                f_request.rejected = f_request.viewed
                f_request.created = timezone.now()
                f_request.viewed = None
                f_request.save()
                return Response(UserSerializer(request.user).data)#"already_rejected"
            else:
                f_request.created = timezone.now()
                f_request.save()
                return Response(UserSerializer(request.user).data)#"created_updated"
        else:
            FriendshipRequest.objects.filter(from_user=to_user, to_user=from_user, viewed__isnull=False).delete()
            return Response(UserSerializer(request.user).data)#"liked"

class PassApiView(APIView):

    def get(self, request):
        to_user = get_object_or_404(user_model, id=request.GET['id'])
        from_user = request.user
        try:
            Friend.objects.add_friend(to_user, from_user)
        except AlreadyExistsError:
            if FriendshipRequest.objects.filter(from_user=to_user, to_user=from_user,
                viewed__isnull=True
            ).exists():
                FriendshipRequest.objects.get(from_user=to_user, to_user=from_user).reject()
                return Response(UserSerializer(request.user).data)#"rejected_or_updated"
            else:
                FriendshipRequest.objects.get(from_user=to_user, to_user=from_user).mark_viewed()
                return Response(UserSerializer(request.user).data)#"viewed_updated"
        else:
            #This is an artificial request created for logging dislike
            #1. when artificial request gets right swipe/ left swipe, handle them
            f_request = FriendshipRequest.objects.get(from_user=to_user, to_user=from_user)
            if FriendshipRequest.objects.filter(from_user=from_user, to_user=to_user,
                viewed__isnull=False
            ).exists():
                FriendshipRequest.objects.get(from_user=from_user, to_user=to_user).delete()
                f_request.reject()
                return Response(UserSerializer(request.user).data)#"both_passed"
            else:
                #delete if any
                FriendshipRequest.objects.filter(from_user=from_user, to_user=to_user).delete()
                f_request.mark_viewed()
                return Response(UserSerializer(request.user).data)#"passed_or_updated"

class ReportApiView(APIView):
    # report fake users
    def get(self, request):
        blocked = get_object_or_404(user_model, id=request.GET['id'])
        blocker = request.user
        try:
            #TODO delete message thread, restrict video calling etc.
            Block.objects.add_block(blocker, blocked, 'P')
        except AlreadyExistsError:
            #TODO log this error and see why it happens
            return Response("already_blocked")
        else:
            return Response("blocked")

    # unmatch
    def post(self, request):
        thread = get_object_or_404(MessageThread, uuid_id=request.POST.get("thread_id", ""))
        reason = request.POST.get("reason", "N")
        if len(reason)>1:
            reason = "N"
        blocker = request.user
        for client in thread.clients.exclude(id=request.user.id):
            try:
                Block.objects.add_block(blocker, client, reason)
            except AlreadyExistsError:
                pass
        thread.is_active = False
        thread.last_activity = timezone.now()
        thread.save()
        return Response("unmatched")

class ProposalsApiView(generics.ListAPIView):

    serializer_class = UserListSerializer
    queryset = user_model.objects.filter(is_active=True, is_new=False, is_superuser=False)

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        finder = self.request.user
        user_is_blocked = Block.objects.blocked(finder)
        user_is_blocking = Block.objects.blocking(finder)
        queryset = queryset.exclude(id__in=user_is_blocked).exclude(id__in=user_is_blocking)
        friends = Friend.objects.friends(finder)
        queryset = queryset.exclude(id__in=friends)
        #TODO see performance of exclude list with cache vs queryset only approach
        last_month = timezone.now().date() - timedelta(days=30)
        #rejected and viewed proposals
        passed_proposals = FriendshipRequest.objects.filter(Q(rejected__gte=last_month)|Q(viewed__gte=last_month), to_user=finder)
        queryset = queryset.exclude(friendship_requests_sent__in=passed_proposals)
        #sent proposals are of two types, real and artifical(where the other user has viewed and passed)
        sent_proposals = FriendshipRequest.objects.filter(created__gte=last_month, viewed__isnull=True, from_user=finder)
        queryset = queryset.exclude(friendship_requests_received__in=sent_proposals)
        # we annotate each object with smaller of two radius:
        # - requesting user
        # - and each user preferred_radius
        # we annotate queryset with distance between given in params location
        # (current_user_location) and each user location
        queryset = queryset.annotate(
            smaller_radius=Least(
                finder.preferred_radius,
                F('preferred_radius'),
                output_field=IntegerField()
            ),
            distance=Distance('last_location', finder.last_location)
        ).order_by(
            'distance'
        )

        queryset = queryset.filter(
            sex=finder.sex if finder.homo else finder.get_opposed_sex,
            preferred_sex=finder.sex,
            age__range=(
                finder.preferred_age_min,
                finder.preferred_age_max),
            preferred_age_min__lte=finder.age,
            preferred_age_max__gte=finder.age,
        ).exclude(
            username=finder.username
        )

        # This is intentional, I want the latest requests first as some people
        # get too many likes. They are probably okay with not seeing the old
        # ones again. For people who get very few likes, any ordering will work.
        q1 = queryset.filter(friendship_requests_sent__to_user=finder).annotate(superlikemessage=ExpressionWrapper(F('friendship_requests_sent__message'),output_field=CharField()))[:10]
        q2 = queryset.exclude(friendship_requests_sent__to_user=finder).annotate(superlikemessage=Value("",output_field=CharField()))[:20]
        return chain(q1,q2)


# get_friendship_context_object_name = lambda: getattr(
#     settings, "FRIENDSHIP_CONTEXT_OBJECT_NAME", "user"
# )
# get_friendship_context_object_list_name = lambda: getattr(
#     settings, "FRIENDSHIP_CONTEXT_OBJECT_LIST_NAME", "users"
# )


# def view_friends(request, username, template_name="friendship/friend/user_list.html"):
#     """ View the friends of a user """
#     user = get_object_or_404(user_model, username=username)
#     friends = Friend.objects.friends(user)
#     return render(
#         request,
#         template_name,
#         {
#             get_friendship_context_object_name(): user,
#             "friendship_context_object_name": get_friendship_context_object_name(),
#         },
#     )


# @login_required
# def friendship_add_friend(
#     request, uuid, template_name="friendship/friend/add.html"
# ):
#     """ Create a FriendshipRequest """
#     ctx = {"uuid": uuid}

#     if request.method == "POST":
#         to_user = user_model.objects.get(username=uuid)
#         from_user = request.user
#         try:
#             Friend.objects.add_friend(from_user, to_user)
#         except AlreadyExistsError as e:
#             ctx["errors"] = ["%s" % e]
#         else:
#             return redirect("friendship_request_list")

#     return render(request, template_name, ctx)


# @login_required
# def friendship_accept(request, friendship_request_id):
#     """ Accept a friendship request """
#     if request.method == "POST":
#         f_request = get_object_or_404(
#             request.user.friendship_requests_received, id=friendship_request_id
#         )
#         f_request.accept()
#         return redirect("friendship_view_friends", username=request.user.username)

#     return redirect(
#         "friendship_requests_detail", friendship_request_id=friendship_request_id
#     )


# @login_required
# def friendship_reject(request, friendship_request_id):
#     """ Reject a friendship request """
#     if request.method == "POST":
#         f_request = get_object_or_404(
#             request.user.friendship_requests_received, id=friendship_request_id
#         )
#         f_request.reject()
#         return redirect("friendship_request_list")

#     return redirect(
#         "friendship_requests_detail", friendship_request_id=friendship_request_id
#     )


# @login_required
# def friendship_cancel(request, friendship_request_id):
#     """ Cancel a previously created friendship_request_id """
#     if request.method == "POST":
#         f_request = get_object_or_404(
#             request.user.friendship_requests_sent, id=friendship_request_id
#         )
#         f_request.cancel()
#         return redirect("friendship_request_list")

#     return redirect(
#         "friendship_requests_detail", friendship_request_id=friendship_request_id
#     )


# @login_required
# def friendship_request_list(
#     request, template_name="friendship/friend/requests_list.html"
# ):
#     """ View unread and read friendship requests """
#     friendship_requests = Friend.objects.requests(request.user)
#     # This shows all friendship requests in the database
#     # friendship_requests = FriendshipRequest.objects.filter(rejected__isnull=True)

#     return render(request, template_name, {"requests": friendship_requests})


# @login_required
# def friendship_request_list_rejected(
#     request, template_name="friendship/friend/requests_list.html"
# ):
#     """ View rejected friendship requests """
#     # friendship_requests = Friend.objects.rejected_requests(request.user)
#     friendship_requests = FriendshipRequest.objects.filter(rejected__isnull=True)

#     return render(request, template_name, {"requests": friendship_requests})


# @login_required
# def friendship_requests_detail(
#     request, friendship_request_id, template_name="friendship/friend/request.html"
# ):
#     """ View a particular friendship request """
#     f_request = get_object_or_404(FriendshipRequest, id=friendship_request_id)

#     return render(request, template_name, {"friendship_request": f_request})


# def followers(request, username, template_name="friendship/follow/followers_list.html"):
#     """ List this user's followers """
#     user = get_object_or_404(user_model, username=username)
#     followers = Follow.objects.followers(user)

#     return render(
#         request,
#         template_name,
#         {
#             get_friendship_context_object_name(): user,
#             "friendship_context_object_name": get_friendship_context_object_name(),
#         },
#     )


# def following(request, username, template_name="friendship/follow/following_list.html"):
#     """ List who this user follows """
#     user = get_object_or_404(user_model, username=username)
#     following = Follow.objects.following(user)

#     return render(
#         request,
#         template_name,
#         {
#             get_friendship_context_object_name(): user,
#             "friendship_context_object_name": get_friendship_context_object_name(),
#         },
#     )


# @login_required
# def follower_add(
#     request, followee_username, template_name="friendship/follow/add.html"
# ):
#     """ Create a following relationship """
#     ctx = {"followee_username": followee_username}

#     if request.method == "POST":
#         followee = user_model.objects.get(username=followee_username)
#         follower = request.user
#         try:
#             Follow.objects.add_follower(follower, followee)
#         except AlreadyExistsError as e:
#             ctx["errors"] = ["%s" % e]
#         else:
#             return redirect("friendship_following", username=follower.username)

#     return render(request, template_name, ctx)


# @login_required
# def follower_remove(
#     request, followee_username, template_name="friendship/follow/remove.html"
# ):
#     """ Remove a following relationship """
#     if request.method == "POST":
#         followee = user_model.objects.get(username=followee_username)
#         follower = request.user
#         Follow.objects.remove_follower(follower, followee)
#         return redirect("friendship_following", username=follower.username)

#     return render(request, template_name, {"followee_username": followee_username})


# def all_users(request, template_name="friendship/user_actions.html"):
#     users = user_model.objects.all()

#     return render(
#         request, template_name, {get_friendship_context_object_list_name(): users}
#     )


# def blocking(request, username, template_name="friendship/block/blockers_list.html"):
#     """ List this user's followers """
#     user = get_object_or_404(user_model, username=username)
#     blockers = Block.objects.blocked(user)

#     return render(
#         request,
#         template_name,
#         {
#             get_friendship_context_object_name(): user,
#             "friendship_context_object_name": get_friendship_context_object_name(),
#         },
#     )


# def blockers(request, username, template_name="friendship/block/blocking_list.html"):
#     """ List who this user follows """
#     user = get_object_or_404(user_model, username=username)
#     blocking = Block.objects.blocking(user)

#     return render(
#         request,
#         template_name,
#         {
#             get_friendship_context_object_name(): user,
#             "friendship_context_object_name": get_friendship_context_object_name(),
#         },
#     )


# @login_required
# def block_add(request, blocked_username, template_name="friendship/block/add.html"):
#     """ Create a following relationship """
#     ctx = {"blocked_username": blocked_username}

#     if request.method == "POST":
#         blocked = user_model.objects.get(username=blocked_username)
#         blocker = request.user
#         try:
#             Block.objects.add_block(blocker, blocked)
#         except AlreadyExistsError as e:
#             ctx["errors"] = ["%s" % e]
#         else:
#             return redirect("friendship_blocking", username=blocker.username)

#     return render(request, template_name, ctx)


# @login_required
# def block_remove(
#     request, blocked_username, template_name="friendship/block/remove.html"
# ):
#     """ Remove a following relationship """
#     if request.method == "POST":
#         blocked = user_model.objects.get(username=blocked_username)
#         blocker = request.user
#         Block.objects.remove_block(blocker, blocked)
#         return redirect("friendship_blocking", username=blocker.username)

#     return render(request, template_name, {"blocked_username": blocked_username})
