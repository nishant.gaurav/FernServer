from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class FriendshipsConfig(AppConfig):
    name = 'bootcamp.friendships'
    verbose_name = _('Friendships')
