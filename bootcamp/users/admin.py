from django import forms
from django.contrib import admin
from django.contrib.gis.db import models
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from bootcamp.users.models import User, UserImage, Job, School, Location, UserRegistrationToken, AppVersion

from mapwidgets.widgets import GooglePointFieldWidget


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)

        except User.DoesNotExist:
            return username

        raise forms.ValidationError(self.error_messages['duplicate_username'])

@admin.register(UserImage)
class UserImageAdmin(admin.ModelAdmin):
    list_display = ("user",)
    list_filter = ("user",)

@admin.register(AppVersion)
class AppVersionAdmin(admin.ModelAdmin):
    list_display = ("code","expiry")
    list_filter = ("code",)

@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    list_display = ("title",)
    list_filter = ("title",)

@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_filter = ("name",)

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ("name",)
    list_filter = ("name",)

@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    formfield_overrides = {
        models.PointField: {"widget": GooglePointFieldWidget}
    }
    fieldsets = (
            ('User Profile', {'fields': ('displayName','birth_date','age','sex','preferred_sex','preferred_age_min','preferred_age_max','last_location','preferred_radius','picture','bio','short_bio','ig_id','ig_access_token','ig_username','ln_id','ln_access_token','ln_url','location','job_title','school','is_new','show_me','swipe_enabled','superlike_enabled','call_enabled','invited_by','likes_count','superlikes_count','count_update_date')}),
    ) + AuthUserAdmin.fieldsets
    list_display = ('username', 'displayName', 'last_name', 'is_superuser', 'invited_by', 'ig_username','age','sex','preferred_sex','preferred_age_min','preferred_age_max','last_location','preferred_radius','app_version')
    list_filter = ('sex','preferred_sex','age','invited_by','app_version','is_new','show_me','swipe_enabled')
    search_fields = ['displayName','last_name','username']

@admin.register(UserRegistrationToken)
class UserRegistrationTokenAdmin(admin.ModelAdmin):
        model = UserRegistrationToken
        list_display = ['registration_token', 'user', 'created']
