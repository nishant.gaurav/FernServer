from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView
from django.http import JsonResponse, HttpResponse, HttpResponseBadRequest
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login
from django.shortcuts import get_object_or_404, redirect
from django.conf import settings
from django.contrib.gis.geos import Point

from bootcamp.users import serializers
from bootcamp.users.models import User, UserImage, UserProof, Location, Job, School, Phone, UserRegistrationToken, AppVersion, AccessToken
from bootcamp.users.signals import user_created
from bootcamp.friendships.signals import friendship_request_accepted

from knox.models import AuthToken
from knox.views import LoginView, LogoutView, LogoutAllView

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ParseError
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status, permissions
from PIL import Image

import os, json, requests


api_version = getattr(settings, 'ACCOUNT_KIT_VERSION')
accountkit_secret = getattr(settings, 'ACCOUNT_KIT_APP_SECRET')
accountkit_app_id = getattr(settings, 'APP_ID')
instagram_client_id = getattr(settings, 'INSTAGRAM_CLIENT_ID')
instagram_client_secret = getattr(settings, 'INSTAGRAM_CLIENT_SECRET')
linkedin_client_id = getattr(settings, 'LINKEDIN_CLIENT_ID')
linkedin_client_secret = getattr(settings, 'LINKEDIN_CLIENT_SECRET')


class ImageUploadView(APIView):
    parser_class = (FileUploadParser,)

    def put(self, request, filename, format=None):
        #filename is compulsory parameter in DRF if Content-Disposition header is missing
        #we are using filename to state the image format
        if 'file' not in request.data:
            raise ParseError("Empty content")
        f = request.data['file']

        try:
            img = Image.open(f)
            img.verify()
        except:
            raise ParseError("Unsupported image type")

        image = UserImage(user=request.user,rank=int(timezone.now().strftime("%s")))
        image.image.save(str(request.user.id)+'/'+str(image.uuid)+'.'+filename, f, save=True)
        request.user.save()
        return Response(serializers.UserImageSerializer(image).data)

class ProofUploadView(APIView):
    parser_class = (FileUploadParser,)

    def put(self, request, filename, format=None):
        #filename is compulsory parameter in DRF if Content-Disposition header is missing
        #we are using filename to state the image format
        if 'file' not in request.data:
            raise ParseError("Empty content")
        f = request.data['file']

        try:
            img = Image.open(f)
            img.verify()
        except:
            raise ParseError("Unsupported image type")

        image = UserProof(user=request.user)
        image.image.save(str(request.user.id)+'/proofs/'+str(image.uuid)+'.'+filename, f, save=True)
        request.user.save()
        return Response(status=status.HTTP_202_ACCEPTED)

class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse('users:detail',
                       kwargs={'uuid': self.request.user.id})


class UserUpdateView(LoginRequiredMixin, UpdateView):
    fields = ['name', 'email', 'picture', 'job_title', 'location', 'personal_url',
              'facebook_account', 'twitter_account', 'github_account',
              'linkedin_account', 'short_bio', 'bio', ]
    model = User

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse('users:detail',
                       kwargs={'uuid': self.request.user.id})

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(id=self.request.user.id)


class UserListView(LoginRequiredMixin, ListView):
    model = User
    # These next two lines tell the view to index lookups by username
    slug_field = 'username'
    slug_url_kwarg = 'username'


class UserValidationView(APIView):

    def get(self, request):
        if 'version_code' in request.GET:
            try:
                app_version = AppVersion.objects.get(code=int(request.GET['version_code']))
                request.user.app_version = app_version
                request.user.save()
            except:
                pass
        return Response(serializers.UserSerializer(request.user).data)

class UserUpdate(APIView):

    def post(self, request):
        user_data = json.loads(request.POST.get("data"))
        serialized = serializers.UserSerializer(request.user, data=user_data, partial=True)
        if serialized.is_valid():
            user = serialized.save()
            if 'city' in user_data:
                city, _ = Location.objects.get_or_create(name=user_data['city'])
                user.location = city
            if 'education' in user_data:
                education, _ = School.objects.get_or_create(name=user_data['education'])
                user.school = education
            if 'job' in user_data:
                job, _ = Job.objects.get_or_create(title=user_data['job'])
                user.job_title = job
            if 'birth_date' in user_data:
                today = timezone.now().date()
                user.age = today.year - user.birth_date.year - ((today.month, today.day) < (user.birth_date.month, user.birth_date.day))
            if 'ig_id' in user_data:
                user.ig_id = user_data['ig_id']
            if 'ig_access_token' in user_data:
                user.ig_access_token = user_data['ig_access_token']
                AccessToken.objects.create(user=user, token=user_data['ig_access_token'], site="instagram")
            if 'ig_username' in user_data:
                user.ig_username = user_data['ig_username']
            if 'ln_id' in user_data:
                user.ln_id = user_data['ln_id']
            if 'ln_access_token' in user_data:
                user.ln_access_token = user_data['ln_access_token']
                AccessToken.objects.create(user=user, token=user_data['ln_access_token'], site="linkedin")
            user.save()
            return Response(status=status.HTTP_202_ACCEPTED)
        else:
            return JsonResponse(serialized.errors,status=status.HTTP_400_BAD_REQUEST)

class ImageDeleteView(APIView):

    def delete(self, request):
        image = get_object_or_404(UserImage, uuid=request.GET['uuid'])
        if image.user == request.user:
            image.delete()
            return Response(serializers.UserSerializer(request.user).data)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

class ImageReorderView(APIView):

    def post(self, request):
        images = json.loads(request.POST.get("images"))
        count = 0
        if request.user.invited_by is None:
            referral_code = request.POST.get("referral_code", "").lower()
            if len(referral_code)>0:
                username = "+" + str(int(referral_code, 36))
                try:
                    inviter = User.objects.get(username=username)
                    request.user.invited_by = inviter
                    request.user.save()
                    if inviter.is_staff:
                        friendship_request_accepted.send(
                            sender=self, from_user=inviter, to_user=request.user
                        )
                except:
                    pass
        for image in images:
            img = get_object_or_404(UserImage, uuid=image)
            img.rank = count
            img.save()
            count += 1
        if (request.user.is_new == False) and (request.user.swipe_enabled == False):
            request.user.is_new = True
            request.user.save()
        return Response(serializers.UserSerializer(request.user).data)

class FCMLogoutView(LogoutView):
    def post(self, request):
        UserRegistrationToken.objects.delete_user_token(request.user)
        return super(FCMLogoutView, self).post(request, format=None)

class FCMLogoutAllView(LogoutAllView):
    def post(self, request):
        UserRegistrationToken.objects.delete_user_token(request.user)
        return super(FCMLogoutAllView, self).post(request, format=None)

class AccountkitLoginView(LoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request):
        if request.user.is_authenticated:
            return Response("Already signed in",status=status.HTTP_403_FORBIDDEN)

        user_access_token = request.POST.get('token', None)
        if user_access_token is None:
            code = request.POST.get('code', None)
            if code is None:
                return Response("Missing authorization code",status=status.HTTP_400_BAD_REQUEST)
            token_url = 'https://graph.accountkit.com/%s/access_token' % api_version
            params = {'grant_type': 'authorization_code', 'code': code,
                    'access_token': 'AA|%s|%s' % (accountkit_app_id, accountkit_secret)
                }

            res = requests.get(token_url, params=params)
            token_response = res.json()

            if 'error' in token_response:
                return Response("Invalid authorization code",status=status.HTTP_400_BAD_REQUEST)

            user_access_token = token_response.get('access_token')

        #Get Account Kit information
        identity_url = 'https://graph.accountkit.com/%s/me' % api_version
        identity_params = {'access_token': user_access_token}

        res = requests.get(identity_url, params=identity_params)
        identity_response = res.json()

        if 'error' in identity_response:
            return Response(identity_response['error']['message'],status=status.HTTP_400_BAD_REQUEST)
        elif identity_response['application']['id'] != accountkit_app_id:
            return Response("Application id is not matching",status=status.HTTP_400_BAD_REQUEST)

        if 'phone' in identity_response:
            phone, _ = Phone.objects.get_or_create(number=identity_response['phone']['number'])
            serialized_phone = serializers.PhoneSerializer(phone, data=identity_response['phone'], partial=True)
            if serialized_phone.is_valid():
                phone = serialized_phone.save()
                if 'id' in identity_response:
                    phone.accountkit_id = identity_response['id']
                    phone.save()
            user = self.get_or_create_user(phone)
            version_code = request.POST.get('version_code', None)
            if version_code is not None:
                app_version = get_object_or_404(AppVersion, code=int(version_code))
                user.app_version = app_version
                user.save()
                if app_version.expiry < timezone.now():
                    return Response({"token":"","user":serializers.UserSerializer(user).data})
            login(request, user)
            return super(AccountkitLoginView, self).post(request, format=None)
        else:
            Response("Only Phone signin is possible",status=status.HTTP_400_BAD_REQUEST)

    def get_or_create_user(self, phone):
        try:
            user = User.objects.get(phone=phone)
        except User.DoesNotExist:
            user = User.objects.create_user(phone=phone, username=phone.number)
            user.set_unusable_password()
            user.is_active = True
            user.save()
            if (User.objects.count() > 1):
                user_created.send(
                    sender=self, from_user=User.objects.get(username="+919004515020"), to_user=user
                )
        return user

class LinkedIn(View):

    def get(self, request, *args, **kwargs):
        if 'code' in request.GET:
            code = request.GET['code']
            token_url = 'https://www.linkedin.com/oauth/v2/accessToken'
            data = {'grant_type': 'authorization_code', 'code': code,
                    'client_id': linkedin_client_id, 'client_secret': linkedin_client_secret,
                    'redirect_uri': f'https://getfern.app/users/~linkedin/'}
            res = requests.post(token_url, data=data)
            token_response = res.json()
            if 'access_token' in token_response:
                return redirect("%s?id=%s&access_token=%s" % (reverse('users:linkedin-linked'),token_response['expires_in'],token_response['access_token']))
            if 'error' in token_response:
                return HttpResponse(token_response['error_description'])
            else:
                return HttpResponse(res.text)
        if 'error' in request.GET:
            return HttpResponse(request.GET['error_description'])
        else:
            return HttpResponseBadRequest()


class Instagram(View):

    def get(self, request, *args, **kwargs):
        if 'code' in request.GET:
            code = request.GET['code']
            token_url = 'https://api.instagram.com/oauth/access_token'
            user = ""
            owner = None
            #TODO: New insta API
            if 'id' in request.GET:
                try:
                    owner = User.objects.get(id=request.GET['id'])
                    user = f"?id={owner.id}"
                except User.DoesNotExist:
                    pass
            data = {'grant_type': 'authorization_code', 'code': code,
                    'client_id': instagram_client_id, 'client_secret': instagram_client_secret,
                    'redirect_uri': f'https://getfern.app/users/~instagram/{user}'}
            res = requests.post(token_url, data=data)
            token_response = res.json()
            if 'access_token' in token_response:
                if owner is not None:
                    owner.ig_access_token = token_response['access_token']
                    owner.ig_username = token_response['user']['username']
                    owner.ig_id = token_response['user']['id']
                    owner.save()
                return redirect("%s?id=%s&access_token=%s&username=%s" % (reverse('users:instagram-linked'),token_response['user']['id'],token_response['access_token'],token_response['user']['username']))
            if 'error_type' in token_response:
                return HttpResponse(token_response['error_message'])
            else:
                return HttpResponse(res.text)
        if 'error' in request.GET:
            return HttpResponse(request.GET['error_description'])
        else:
            return HttpResponseBadRequest()


class UpdateLocationView(APIView):

    def post(self, request):
        lat = json.loads(request.POST.get("latitude"))
        lon = json.loads(request.POST.get("longitude"))
        if (lat>90 or lat<-90 or lon>180 or lon<-180):
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
        request.user.last_location = Point(lon,lat)
        request.user.save()
        return Response(status=status.HTTP_202_ACCEPTED)

class DatesReorderView(APIView):

    def post(self, request):
        dates = json.loads(request.POST.get("dates"))
        arr = [2,1,0,-1,-2]
        score = 2
        for date in dates:
            if date < 0 or date >= 5:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            arr[date] = score
            score -= 1
        request.user.date_rank_array = arr
        request.user.preferred_age_min = json.loads(request.POST.get("min_age"))
        request.user.preferred_age_max = json.loads(request.POST.get("max_age"))
        request.user.save()
        return Response(serializers.UserSerializer(request.user).data)


#MARK: FCM

class RegistrationToken(APIView):

    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        self.check_object_permissions(request, None)

        token = request.data.get('token', None)
        if token is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        elif token == "":
            return Response(status=status.HTTP_400_BAD_REQUEST)

        if request.user.is_new and request.user.swipe_enabled:
            request.user.is_new = False
        request.user.save()

        UserRegistrationToken.objects.create_and_set_user_token(request.user, token)
        return Response(status=status.HTTP_201_CREATED)

    def delete(self, request, *args, **kwargs):
        self.check_object_permissions(request, None)

        UserRegistrationToken.objects.delete_user_token(request.user)
        return Response(status=status.HTTP_200_OK)
